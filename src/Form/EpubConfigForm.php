<?php

namespace Drupal\epub_module\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Controller to render Epub Config Form.
 */
class EpubConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'epub_module.epubsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'epub_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('epub_module.epubsettings');
    $moduleHandler = \Drupal::service('module_handler');
    $form['background_color'] = [
      '#type' => $moduleHandler->moduleExists('color_field') ? 'color' : 'textfield',
      '#title' => $this->t('Background Color for Epub Viewer'),
      '#description' => $this->t('Background Color to display in epub viewer to users when they read epub files.'),
      '#default_value' => $config->get('background_color'),
    ];
    $form['icon_color'] = [
      '#type' => $moduleHandler->moduleExists('color_field') ? 'color' : 'textfield',
      '#title' => $this->t('Icon Color for Epub Viewer'),
      '#description' => $this->t('Icon Color to display on epub viewer.'),
      '#default_value' => $config->get('icon_color'),
    ];
    $form['font_color'] = [
      '#type' => $moduleHandler->moduleExists('color_field') ? 'color' : 'textfield',
      '#title' => $this->t('Font Color for Epub Viewer'),
      '#description' => $this->t('Font Color to display in epub viewer.'),
      '#default_value' => $config->get('font_color'),
    ];
    $form['show_download_icon'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Download Icon in Epub Viewer'),
      '#description' => $this->t('Check To Display Download Icon in Epub Viewer'),
      '#default_value' => $config->get('show_download_icon'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('epub_module.epubsettings')->set('background_color', $form_state->getValue('background_color'))->set('icon_color', $form_state->getValue('icon_color'))->set('font_color', $form_state->getValue('font_color'))->set('show_download_icon', $form_state->getValue('show_download_icon'))->save();
  }

}
