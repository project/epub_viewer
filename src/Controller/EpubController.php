<?php

namespace Drupal\epub_module\Controller;

use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for rendering epub file to the ebook viewer.
 */
class EpubController {

  /**
   * Function for rendering epub file to the ebook viewer.
   */
  public function viewEbook($fid = FALSE) {
    if ($fid) {
      global $base_url;
      $data = [];
      // Loading file from file ID.
      $file = File::load($fid);
      $file_uri = $file->getFileUri();
      $file_url = Url::fromUri(\Drupal::service('file_url_generator')->generateAbsoluteString($file_uri));
      $data['file_path'] = $file_url->getUri();
      $data['base_url'] = $base_url;
      $config = \Drupal::config('epub_module.epubsettings');
      $options = [
        'background_color' => $config->get('background_color'),
        'icon_color' => $config->get('icon_color'),
        'font_color' => $config->get('font_color'),
        'show_download_icon' => $config->get('show_download_icon'),
      ];
      $module_path = \Drupal::service('extension.list.module')->getPath('epub_module');
      $data['module_path'] = $base_url . '/' . $module_path;
      $build = [
        '#theme' => 'epub_view',
      // Sending file data to the viewer.
        '#data' => $data,
        '#options' => $options,
      ];
      $html = \Drupal::service('renderer')->renderRoot($build);
      $response = new Response();
      $response->setContent($html);
      return $response;
    }
  }

}
