<?php

namespace Drupal\epub_module\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'epub_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "epub_field_formatter",
 *   label = @Translation("Epub Formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class EpubFieldFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   * Creates a link for epub files that redirects to the ebook viewer.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    global $base_url;
    $elements = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      if ($file->getMimeType() == 'application/epub+zip') {
        $path = $base_url . '/view-ebook/' . $file->id();
        $elements[$delta] = [
          '#theme' => 'epub_formatter',
          '#path' => $path,
        ];
      }
      else {
        $elements[$delta] = [
          '#theme' => 'file_link',
          '#file' => $file,
        ];
      }
    }
    return $elements;
  }

}
