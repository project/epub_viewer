function epub_loader(epub_setting){
    var myBook = ePub(epub_setting.file_path,{
                    restore: true,
                });
    if (myBook.archived) {
        var rendition = myBook.renderTo("viewer");
        var hash = window.location.hash.slice(2);
        var displayed = rendition.display(hash || undefined);

        jQuery( "#controls" ).slider({
            min: 0,
            max: 100,
            step: 1,
            value: 0,
            slide: function( event, ui ) {
                jQuery( "#pageIndicator" ).val(ui.value);
                var cfi = myBook.locations.cfiFromPercentage(ui.value / 100);
                var page = myBook.pageList.percentageFromCfi(cfi);
                rendition.display(cfi);
            }
        });

        jQuery("#iframe_div").on('click',function(e){
            e.preventDefault();
            jQuery("#titlebar").toggle();
            jQuery("#footer").toggle();
        });

        jQuery("#prev").on('click',function(e){
            e.preventDefault();
            rendition.prev();
        });

        jQuery("#next").on('click',function(e){
            e.preventDefault();
            rendition.next();
        });

        jQuery("#slider").on('click',function(e){
            e.preventDefault();
            jQuery("#sidebar").css("z-index","9999");
            jQuery("#sidebar").css("overflow","scroll");
            jQuery("#footer").css("left","10.5%");
            jQuery("#controls").css("width","79.5%");
        });

        jQuery("#hide_menu").on("click",function(e){
            e.preventDefault();
            jQuery("#sidebar").css("z-index","0");
            jQuery("#footer").css("left","0%");
            jQuery("#controls").css("width","90%");
        });

        jQuery(window).on( "swipeleft", function( event ) {
            rendition.next();
        });
        jQuery(window).on( "swiperight", function( event ) {
            rendition.prev();
        }); 
        jQuery("#controls").css("background",epub_setting.icon_color);
        jQuery(".back").css("background",epub_setting.background_color);
        jQuery(".font").css("color",epub_setting.font_color);        
        jQuery(".icon").css("color",epub_setting.icon_color);
        jQuery("head").append('<style>#controls span.ui-slider-handle::before{background-image: linear-gradient(to bottom,'+ epub_setting.icon_color + ' 0%,' + epub_setting.icon_color + ' 100%);}#footer{border-color:' + epub_setting.icon_color +'}#titlebar{border-bottom: 1px solid' + epub_setting.icon_color +'}div#prev:before,div#next:before{color: ' + epub_setting.icon_color + ' !important}#toc ul li a span{color:' + epub_setting.font_color +'}</style>');

        if(typeof screenfull !== 'undefined') {
            $fullscreen = jQuery("#fullscreen");
            $fullscreen.on("click", function() {
                screenfull.toggle(jQuery('#container')[0]);
            });
            if(screenfull.raw) {
                document.addEventListener(screenfull.raw.fullscreenchange, function() {
                    fullscreen = screenfull.isFullscreen;
                    if(fullscreen) {
                        $fullscreen
                          .addClass("icon-resize-small")
                          .removeClass("icon-resize-full");
                    } else {
                        $fullscreen
                          .addClass("icon-resize-full")
                          .removeClass("icon-resize-small");
                    }
                });
            }
        }
        var currentPage = document.getElementById("pageIndicator");
        var mouseDown = false;
        var keyListener = function(e){
            if ((e.keyCode || e.which) == 37) {
                rendition.prev();
            }
            if ((e.keyCode || e.which) == 39) {
                rendition.next();
            }
        };

        rendition.on("keyup", keyListener);
        document.addEventListener("keyup", keyListener, false);

        rendition.on("rendered", function(section){
            var nextSection = section.next();
            var prevSection = section.prev();

            if(nextSection) {
                nextNav = myBook.navigation.get(nextSection.href);
            } else {
                next.textContent = "";
            }

            if(prevSection) {
                prevNav = myBook.navigation.get(prevSection.href);
            } else {
                prev.textContent = "";
            }
        });

        rendition.on("relocated", function(location){
	        if(location.start.href == 'cover.xml' || location.start.href == 'cover.xhtml' || location.start.href == 'cover.html'){
	            jQuery('#viewer').removeClass('content_class').addClass('cover_class');
                jQuery(".cover_class").css("background",epub_setting.background_color);
	        }else{
	            jQuery('#viewer').removeClass('cover_class').addClass('content_class');
                jQuery(".content_class").css("background",'');
	        }
        });

        rendition.themes.default({
          body: {
            color: epub_setting.font_color
          },
        });

        myBook.ready.then(function(){
            var key = myBook.key()+'-locations';
            var stored = localStorage.getItem(key);
            if (stored) {
                return myBook.locations.load(stored);
            } else {
                return myBook.locations.generate(1600);
            }
        })
        .then(function(locations){
        // Wait for book to be rendered to get current page
            displayed.then(function(){
                // Get the current CFI
                var currentLocation = rendition.currentLocation();
                // Get the Percentage (or location) from that CFI
                var currentPage = myBook.locations.percentageFromCfi(currentLocation.start.cfi);
                jQuery("#controls").slider("value",parseInt(currentPage));
                currentPage.value = currentPage;
            });

            //controls.appendChild(slider);
            currentPage.addEventListener("change", function(){
                var cfi = myBook.locations.cfiFromPercentage(currentPage.value/100);
                rendition.display(cfi);
            }, false);

            // Listen for location changed event, get percentage from CFI
            rendition.on('relocated', function(location){
                var percent = myBook.locations.percentageFromCfi(location.start.cfi);
                var percentage = Math.floor(percent * 100);
                if(!mouseDown) {
                    jQuery("#controls").slider("value",parseInt(percentage));
                }
                currentPage.value = percentage;
            });
            // Save out the generated locations to JSON
            localStorage.setItem(myBook.key()+'-locations', myBook.locations.save());
        });

        myBook.loaded.metadata.then(function(meta) {
            var title = meta.title,
            author = meta.creator;
            var $title = jQuery("#book-title"),
            $author = jQuery("#chapter-title"),
            $dash = jQuery("#title-seperator");
            document.title = title+" – "+author;
            $title.html(title);
            $author.html(author);
            $dash.show();
        });

        myBook.loaded.navigation.then(function(toc){
            var $nav = document.getElementById("toc"),
                docfrag = document.createDocumentFragment();
            var addTocItems = function (parent, tocItems) {
                var $ul = document.createElement("ul");
                tocItems.forEach(function(chapter) {
                    var item = document.createElement("li");
                    var link = document.createElement("a");
                    var lbl = document.createElement("span");
                    //link.textContent = chapter.label;
                    lbl.textContent= chapter.label;
                    link.appendChild(lbl);
                    link.href = chapter.href;
                    item.appendChild(link);

                    if (chapter.subitems) {
                        addTocItems(item, chapter.subitems)
                    }

                    link.onclick = function(){
                        var url = link.getAttribute("href");
                        rendition.display(url);
                        return false;
                    };

                    $ul.appendChild(item);
                });
                parent.appendChild($ul);
            };

            addTocItems(docfrag, toc);

            $nav.appendChild(docfrag);

            if ($nav.offsetHeight + 60 < window.innerHeight) {
                $nav.classList.add("fixed");
            }

        });

        myBook.loaded.metadata.then(function(meta){
            var $title = document.getElementById("title");
            var $author = document.getElementById("author");
            var $cover = document.getElementById("cover");

            $title.textContent = meta.title;
            $author.textContent = meta.creator;
            if (myBook.archive) {
                myBook.archive.createUrl(myBook.cover)
                .then(function (url) {
                    $cover.src = url;
                })
            } else {
                $cover.src = myBook.cover;
            }
        });        
    }else{
        //window.location.href = "page-not-found-404";
        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        jQuery(location).attr('href', baseUrl+'/access-error/file_not_found');
    }
}