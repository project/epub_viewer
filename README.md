# Description :
1. This module mainly designed for Drupal 8 enables users to view ebooks(.epub files) in the browser.
2. This module creates a field formatter for epub files and generates a link for the epub file that redirects to the ebook viewer. You will need to select field formatter(EPUBFormatter) for the epub file upload field from the field settings.
3. For customization of the epub viewer settings will be available at URL /admin/config/epub/epubsettings.
4. All the files required for handling epub files are in this module so there is no need to download an external library for it.
5. Let us know if you want any additional setting/feature.

# Installation :
* Enable the module from Extend menu.
* Create file upload field for uploading epub files.
* Select Epub Formatter from the field settings.
* Upload epub files and view the node created.
* Go to admin section, select settings for customization of epub viewer. If jQuery colorpicker module is enabled then colour settings fields will appear otherwise textbox will appear in the settings.
* Click on the link generated for the epub files.
* Redirect to the epub viewer which will open your epub file.
